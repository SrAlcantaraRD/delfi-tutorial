import FileDownloadIcon from "@mui/icons-material/FileDownload";
import FileUploadIcon from "@mui/icons-material/FileUpload";
import DownloadForOfflineIcon from "@mui/icons-material/DownloadForOffline";
import { Button, Divider } from "@mui/material";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import * as React from "react";
import { FC, useEffect, useState } from "react";
import { getFromWeiToString, IAccountBalanceRow } from "../pages/utils";
import {
  DaiTokenInstance,
  DappTokenInstance,
  TokenFarmInstance,
} from "../smart-contracts/types";
import StakeModal from "./stakeModal";

interface AddressBalanceTableProps {
  web3: Web3;
  accounts: string[];
  daiTokenContract: DaiTokenInstance;
  dappTokenContract: DappTokenInstance;
  tokenFarmContract: TokenFarmInstance;
}

const AddressBalanceTable: FC<AddressBalanceTableProps> = ({
  web3,
  accounts,
  daiTokenContract,
  dappTokenContract,
  tokenFarmContract,
}): JSX.Element => {
  const [rows, setRows] = useState<IAccountBalanceRow[]>([]);
  const [isOpenStalkingModal, setIsOpenStalkingModal] =
    useState<boolean>(false);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [selectedAccount, setSelectedAccount] =
    useState<IAccountBalanceRow>(null);

  useEffect(() => {
    loadAccountData();
  }, [accounts]);

  const loadAccountData = async () => {
    if (!accounts?.length) return;
    await Promise.all(
      accounts.map(async (account) => createData(account))
    ).then((_rows) => setRows(_rows));
  };

  const createData = async (account: string): Promise<IAccountBalanceRow> => {
    if (!daiTokenContract || !tokenFarmContract || !dappTokenContract) return;

    const balance = await getAccountBalance(account, daiTokenContract);
    const aprove = await daiTokenContract.methods
      .allowance(account, tokenFarmContract._address)
      .call();
    const rewardBalance = await getAccountBalance(account, dappTokenContract);
    const stakingBalance = await getAccountBalance(
      account,
      tokenFarmContract,
      "stakingBalance"
    );

    // log
    return {
      account,
      balance,
      aprove: web3.utils?.fromWei(aprove, "ether"),
      stakingBalance,
      rewardBalance,
    };
  };

  const getAccountBalance = async (
    _account: string,
    _contract,
    _funName: "balanceOf" | "stakingBalance" = "balanceOf"
  ): Promise<string> => {
    const balance: string = await _contract.methods[_funName](_account).call();

    return web3.utils?.fromWei(balance, "ether");
  };

  const openStalkingModal = (row: IAccountBalanceRow) => {
    setSelectedAccount(row);
    setIsOpenStalkingModal(true);
  };

  const stakeTokens = async (investor, tokens) => {
    setIsLoading(true);

    try {
      await daiTokenContract.methods
        .approve(tokenFarmContract._address, tokens)
        .send({ from: investor })
        .on("transactionHash", async (hash) => console.log({ hash }));

      await tokenFarmContract.methods
        .stakeTokens(tokens)
        .send({ from: investor })
        .on("transactionHash", (hash) => console.log({ hash }));

      await loadAccountData();
      setIsLoading(false);
    } catch (error) {
      console.log({ error });
    }
    // daiTokenContract,
    // dappTokenContract,
  };

  const unstakeTokens = async (investor: string) => {
    try {
      setIsLoading(true);

      await tokenFarmContract.methods
        .unstakeTokens()
        .send({ from: investor })
        .on("transactionHash", async (hash) => console.log({ hash }));

      await loadAccountData();
      setIsLoading(false);
    } catch (error) {
      console.warn({ error });
    }
  };

  return (
    <>
      <TableContainer component={Paper}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Account</TableCell>
              <TableCell align="right">Balance</TableCell>
              {/* <TableCell align="right">Aprove</TableCell> */}
              <TableCell align="right">Staking Balance</TableCell>
              <TableCell align="right">Reward Balance</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <TableRow
                selected={row.account == selectedAccount?.account}
                key={row.account}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell component="th" scope="row">
                  {row.account}
                </TableCell>
                <TableCell align="right">
                  {getFromWeiToString(web3, row.balance)}
                </TableCell>
                {/* <TableCell align="right">
                  {getFromWeiToString(web3, row.aprove)}
                </TableCell> */}
                <TableCell align="right">
                  {getFromWeiToString(web3, row.stakingBalance)}
                </TableCell>
                <TableCell align="right">
                  {getFromWeiToString(web3, row.rewardBalance)}
                </TableCell>
                <TableCell align="center">
                  <Button
                    size="small"
                    color="success"
                    startIcon={<FileDownloadIcon color={"success"} />}
                    onClick={() => openStalkingModal(row)}
                  />
                  {/* <Divider orientation="vertical" flexItem /> */}

                  <Button
                    size="small"
                    color="success"
                    startIcon={<FileUploadIcon color={"success"} />}
                    onClick={() => unstakeTokens(row.account)}
                  />
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <StakeModal
        {...{
          web3,
          isOpen: isOpenStalkingModal,
          targetAccount: selectedAccount,
          handleClose: () => setIsOpenStalkingModal(false),
          stakeTokens,
        }}
      />
    </>
  );
};

export default AddressBalanceTable;
