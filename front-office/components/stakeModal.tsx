import { Box, Button, Modal, TextField, Typography } from "@mui/material";
import * as React from "react";
import { FC, useState } from "react";
import { getFromWeiToString, IAccountBalanceRow } from "../pages/utils";
import { DaiTokenInstance, DappTokenInstance } from "../smart-contracts/types";

interface StakeModalProps {
  isOpen: boolean;
  web3: Web3;
  targetAccount: IAccountBalanceRow;
  handleClose: () => void;
  // daiTokenContract: DaiTokenInstance;
  // dappTokenContract: DappTokenInstance;
  stakeTokens: (account: any, ammount: any) => Promise<void>;
}

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  //   width: 600,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

const StakeModal: FC<StakeModalProps> = ({
  web3,
  isOpen,
  targetAccount,
  handleClose,
  stakeTokens,
}): JSX.Element => {
  const [ammount, setAmmount] = useState<string>("0");

  const handleStakeTokens = async () => {
    const balance = web3.utils.toWei(ammount, "ether");
    await stakeTokens(targetAccount.account, balance);
    handleClose();
  };

  return (
    <Modal
      open={isOpen}
      onClose={handleClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box sx={style}>
        <Typography
          id="modal-modal-title"
          variant="h6"
          component="h2"
          style={{ marginBottom: 20 }}
        >
          Stake Modal
        </Typography>
        <TextField
          onChange={(e) => setAmmount(e.target.value)}
          id="standard-basic"
          label={`Maxim ${getFromWeiToString(
            web3,
            targetAccount?.balance || "0"
          )}`}
          variant="standard"
          value={ammount}
        />
        <Button variant="outlined" size="small" onClick={handleStakeTokens}>
          Stake
        </Button>
      </Box>
    </Modal>
  );
};

export default StakeModal;
