export interface IAccountBalanceRow {
  account: string;
  balance: string;
  aprove: string;
  stakingBalance: string;
  rewardBalance: string;
}

export const getFromWeiToString = (web3: Web3, balance: string): string => {
  if (!web3) return "KO";

  return `${balance} ethers`;
  // return `${web3.utils?.fromWei(balance, "ether")} ethers`;
};
