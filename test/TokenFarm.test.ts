import {
  DaiTokenInstance,
  DappTokenInstance,
  TokenFarmInstance,
} from "../smart_contracts/types";

const DaiToken = artifacts.require("DaiToken");
const DappToken = artifacts.require("DappToken");
const TokenFarm = artifacts.require("TokenFarm");

// import chai = require("chai");
// import chaiAsPromised = require("chai-as-promised");
// chai.use(chaiAsPromised);

// const expect = chai.expect;

require("chai").use(require("chai-as-promised")).should();

function tokens(amount: number) {
  return web3.utils.toWei(amount.toString(), "ether");
}

contract("TokenFarm", ([owner, investor]) => {
  let daiToken: DaiTokenInstance;
  let dappToken: DappTokenInstance;
  let tokenFarm: TokenFarmInstance;

  before(async () => {
    daiToken = await DaiToken.new();
    dappToken = await DappToken.new();
    tokenFarm = await TokenFarm.new(dappToken.address, daiToken.address);

    //   Transferir todos los tokets a Farm
    await dappToken.transfer(tokenFarm.address, tokens(1000000));

    // Enviar tokens al inversor
    await daiToken.transfer(investor, tokens(100), {
      from: owner,
    });
  });

  // write test here
  describe("Mock Dai Deployment", async () => {
    it("Has a name", async () => {
      const name = await daiToken.name();

      assert.equal(name, "Mock DAI Token");
    });
  });

  // write test here
  describe("Dapp Token Deployment", async () => {
    it("Has a name", async () => {
      const name = await dappToken.name();

      assert.equal(name, "DApp Token");
    });
  });

  // write test here
  describe("Token Farm Deployment", async () => {
    it("Has a name", async () => {
      const name = await tokenFarm.name();

      assert.equal(name, "Dapp Token Farm");
    });

    it("Has tokens", async () => {
      let balance = await dappToken.balanceOf(tokenFarm.address);

      assert.equal(balance.toString(), tokens(1000000));
    });
  });

  describe("Farming tokens", async () => {
    it("Rewards investors for staaking mDai tokens", async () => {
      const result = await daiToken.balanceOf(investor);

      assert.equal(
        result.toString(),
        tokens(100),
        "Investor Mock DAI wallet balance before staking"
      );

      // Take Mock DAI Token
      await daiToken.approve(tokenFarm.address, tokens(100), {
        from: investor,
      });

      await tokenFarm.stakeTokens(tokens(100), { from: investor });
    });

    // Check staking result
    it("Mock DAI walle balance correct after staking", async () => {
      const result = await daiToken.balanceOf(investor);

      assert.equal(
        result.toString(),
        tokens(0),
        "Investor Mock DAI walle balance correct after staking"
      );
    });

    it("Token Farm Mock DAI walle balance correct after staking", async () => {
      const result = await daiToken.balanceOf(tokenFarm.address);

      assert.equal(
        result.toString(),
        tokens(100),
        "Token Farm Mock DAI balance correct after staking"
      );
    });

    it("Investor stalking balance is correct", async () => {
      const result = await tokenFarm.stakingBalance(investor);

      assert.equal(
        result.toString(),
        tokens(100),
        "Investor stalking balance is correct"
      );
    });

    it("Investor is stalking Mock DAI token", async () => {
      const result = await tokenFarm.isStalking(investor);

      assert.equal(result, true, "Investor stalking balance is correct");
    });

    it("Issuance only can be called by owner", async () => {
      // @ts-ignore
      await tokenFarm.issueTokens({ from: investor }).should.be.rejected;
    });

    it("Check balance after issuance Token on DApp", async () => {
      await tokenFarm.issueTokens({ from: owner });
      const result = await dappToken.balanceOf(investor);

      assert.equal(
        result.toString(),
        tokens(100),
        "Investor DApp token balance is correct after issuance Token"
      );
    });

    it("Unstake investor tokens", async () => {
      await tokenFarm.unstakeTokens({ from: investor });

      let result = await tokenFarm.stakingBalance(investor);

      assert.equal(
        result.toString(),
        tokens(0),
        "Investor staking balance is correct after issuance Token"
      );

      result = await daiToken.balanceOf(investor);

      assert.equal(
        result.toString(),
        tokens(100),
        "Investor DApp token balance is correct after unstaking"
      );

      result = await daiToken.balanceOf(tokenFarm.address);

      assert.equal(
        result.toString(),
        tokens(0),
        "Token Farm Mock DAI balance correct after unStaking"
      );

      const _result = await tokenFarm.isStalking(investor);

      assert.equal(_result, false, "Investor stalking balance is correct");
    });
  });
});
