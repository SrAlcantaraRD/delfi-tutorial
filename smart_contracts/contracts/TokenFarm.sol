// SPDX-License-Identifier: MIT
pragma solidity 0.8.4;

import "./DaiToken.sol";
import "./DappToken.sol";

contract TokenFarm {
    string public name = "Dapp Token Farm";
    DappToken public dappToken;
    DaiToken public daiToken;
    address public owner;

    address[] public stakers;
    mapping(address => uint256) public stakingBalance;
    mapping(address => bool) public hasStaked;
    mapping(address => bool) public isStalking;

    constructor(DappToken _dappToken, DaiToken _daiToken) {
        dappToken = _dappToken;
        daiToken = _daiToken;
        owner = msg.sender;
    }

    // 1. Stake Tokens (Deposit)
    function stakeTokens(uint256 _amount) public {
        require(_amount > 0, "'amount' need to be greather than 0");

        // Transfer Mock Dai tokens to this contract for staking
        daiToken.transferFrom(msg.sender, address(this), _amount);

        // Update staking balance
        stakingBalance[msg.sender] = stakingBalance[msg.sender] + _amount;

        // Add user to stakers array *only* if they haven't staked already
        if (!hasStaked[msg.sender]) {
            stakers.push(msg.sender);
        }

        // Update staking status
        hasStaked[msg.sender] = true;
        isStalking[msg.sender] = true;
    }

    // 2. Unstaking Tokens (Withdraw)
    function unstakeTokens() public {
        require(isStalking[msg.sender], "address is not staking");

        uint256 balance = stakingBalance[msg.sender];

        require(balance > 0, "stake balance must be greather than 0");

        daiToken.transfer(msg.sender, balance);

        // Update staking balance
        stakingBalance[msg.sender] = 0;

        isStalking[msg.sender] = false;
    }

    // 3. Issuing Tokens
    function issueTokens() public {
        require(msg.sender == owner, "caller must be owner");

        for (uint256 idx = 0; idx < stakers.length; idx++) {
            address recipient = stakers[idx];
            uint256 balance = stakingBalance[recipient];

            if (balance > 0) {
                dappToken.transfer(recipient, balance);
            }
        }
    }
}
