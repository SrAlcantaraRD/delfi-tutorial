var DappToken = artifacts.require("DappToken");
var DaiToken = artifacts.require("DaiToken");
var TokenFarm = artifacts.require("TokenFarm");

module.exports = async function (deployer, network, accounts) {
  // Deploy Mock Dai Token
  await deployer.deploy(DaiToken);
  var daiToken = await DaiToken.deployed();

  // Deploy Mock Dapp Token
  await deployer.deploy(DappToken);
  var dappToken = await DappToken.deployed();

  // Deploy Mock Dapp Token
  await deployer.deploy(TokenFarm, dappToken.address, daiToken.address);
  var tokenFarm = await TokenFarm.deployed();

  // Transfer all tokens from DappToken to TokenFarm => 1 million
  await dappToken.transfer(tokenFarm.address, "1000000000000000000000000");

  // Transfer 100 mock tokens to investor
  await daiToken.transfer(accounts[1], "10000000000000000000");
} as Truffle.Migration;

export {};
