const path = require("path");
require("ts-node").register({
  files: true,
});

module.exports = {
  // See https://trufflesuite.com/docs/truffle/reference/configuration#networks
  // to customize your Truffle configuration!
  // contracts_build_directory: path.join(__dirname, "contracts"),
  networks: {
    // develop: {
    //   port: 8545,
    // },
    development: {
      host: "127.0.0.1",
      port: 7545,
      network_id: "5777", // Match any network id
    },
  },
  contracts_directory: path.join(__dirname, "smart_contracts/contracts/"), //lo colocamos aquí para que estén expuetos a front-office
  contracts_build_directory: path.join(__dirname, "smart_contracts/build/"), //lo colocamos aquí para que estén expuetos a front-office
  migrations_directory: path.join(__dirname, "smart_contracts/migrations/"),
  compilers: {
    solc: {
      optimizer: {
        enabled: true,
        runs: 200,
      },
      // evmVersion: "petersburg",
      version: "0.8.4",
    },
  },
};
